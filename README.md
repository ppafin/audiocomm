# audiocomm

Simple controllable audio recorder with gstreamer. Allows user to 
control recording with signals. 

Based on gstreamer examples and gstreamer-cookbook examples. 

See also:

* https://github.com/crearo/gstreamer-cookbook/blob/master/C/dynamic-recording.c
* https://stackoverflow.com/questions/47550863/gstreamer-split-audio-stream-into-files

