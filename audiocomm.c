 /* 
  * audiocomm - simple audio recorder with gstreamer
  * Copyright (C) 2021 Pasi Patama, <pasi@patama.net>
  * 
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  * 
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
  * 
  */

#include <string.h>
#include <gst/gst.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>

#include "audiocomm.h"
#include "ini.h"
#include "log.h"

#define INI_FILE "audiocomm.ini"
char *targetdirectory = "";

static GMainLoop *loop;
static GstElement *pipeline, *src,*src_audioconvert,*src_resample, *tee, *encoder, *muxer, *filesink, *audioconvert, *fakesink, *queue_record, *queue_fakesink;
static GstBus *bus;
static GstPad *teepad;
static gboolean recording = FALSE;
static gboolean unlinked = FALSE;

static gboolean msghandle (GstBus * l_bus, GstMessage * message, gpointer user_data)
{
	GError *err = NULL;
	gchar *name, *debug = NULL;

	switch (GST_MESSAGE_TYPE (message)) {
		case GST_MESSAGE_ERROR:

			name = gst_object_get_path_string (message->src);
			gst_message_parse_error (message, &err, &debug);
			log_error("[%d] ERROR: from element %s: %s ", getpid(), name, err->message);
			if (debug != NULL)
				log_error("[%d] Debug info: %s ", getpid(),debug);

			g_error_free (err);
			g_free (debug);
			g_free (name);
			g_main_loop_quit (loop);
			break;
		case GST_MESSAGE_WARNING:
			name = gst_object_get_path_string (message->src);
			gst_message_parse_warning (message, &err, &debug);
			log_error("[%d] ERROR: from element %s: %s ", getpid(),name, err->message);
			if (debug != NULL)
				log_error("[%d] Debug info: %s ", getpid(),debug);

			g_error_free (err);
			g_free (debug);
			g_free (name);
			break;
		case GST_MESSAGE_EOS:
			log_info("[%d] Got EOS", getpid());
			g_main_loop_quit (loop);
			gst_element_set_state (pipeline, GST_STATE_NULL);
			g_main_loop_unref (loop);
			gst_object_unref (pipeline);
			exit(0);
			break;
		default:
			break;
	}

	return TRUE;
}

static GstPadProbeReturn padunlink(GstPad *pad, GstPadProbeInfo *info, gpointer user_data) {
	log_info("[%d] Unlinking", getpid());
	GstPad *sinkpad;
	sinkpad = gst_element_get_static_pad (queue_record, "sink");
	gst_pad_unlink (teepad, sinkpad);
	gst_object_unref (sinkpad);
	gst_element_send_event(encoder, gst_event_new_eos()); 
	gst_bin_remove(GST_BIN (pipeline), queue_record);
	gst_bin_remove(GST_BIN (pipeline), encoder);
	gst_bin_remove(GST_BIN (pipeline), muxer);
	gst_bin_remove(GST_BIN (pipeline), filesink);
	gst_element_set_state(queue_record, GST_STATE_NULL);
	gst_element_set_state(encoder, GST_STATE_NULL);
	gst_element_set_state(muxer, GST_STATE_NULL);
	gst_element_set_state(filesink, GST_STATE_NULL);
	gst_object_unref(queue_record);
	gst_object_unref(encoder);
	gst_object_unref(muxer);
	gst_object_unref(filesink);
	gst_element_release_request_pad (tee, teepad);
	gst_object_unref (teepad);
	log_info("[%d] Unlinked", getpid());
	unlinked = TRUE; 
	return GST_PAD_PROBE_REMOVE;
}

void stopfile(void) {
	log_info("[%d] Record stop", getpid());  
		gst_element_set_state(pipeline, GST_STATE_PAUSED);
	gst_pad_add_probe(teepad, GST_PAD_PROBE_TYPE_IDLE, padunlink, NULL, (GDestroyNotify) g_free);
	recording = FALSE;
}

char *time_stamp(){
	char *timestamp = (char *)malloc(sizeof(char) * 18);
	memset(timestamp,0,18);
	time_t ltime;
	ltime=time(NULL);
	struct tm *tm;
	tm=localtime(&ltime);
	sprintf(timestamp,"%04d%02d%02d_%02d%02d%02d", tm->tm_year+1900, tm->tm_mon, 
		tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
	return timestamp;
}

void startfile(void) {
	log_info("[%d] Record start", getpid()); 
		gst_element_set_state(pipeline, GST_STATE_PLAYING);
	GstPad *sinkpad;
	GstPadTemplate *templ;
	templ = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(tee), "src_%u");
	teepad = gst_element_request_pad(tee, templ, NULL, NULL);
	queue_record = gst_element_factory_make("queue", "queue_record");
	encoder = gst_element_factory_make("opusenc", NULL);
	g_object_set (G_OBJECT ( encoder ), "bitrate", 16000, NULL); 	// 6000 - 128000
	g_object_set (G_OBJECT ( encoder ), "audio-type", 2048, NULL);	// 2048 = voice, 2049 = Generic
	g_object_set (G_OBJECT ( encoder ), "bandwidth", 1103, NULL);	// 1101 narrowband 1102 medium band fullband (1105) 
	muxer = gst_element_factory_make("oggmux", NULL);
	filesink = gst_element_factory_make("filesink", NULL);
	
	/* Dynamic vs static filename depends on application type */
	char *file_name = (char*) malloc(255 * sizeof(char));
	// sprintf(file_name, "%s.opus", time_stamp() ); // dynamic
	sprintf(file_name, "out.opus" ); // static
	
	log_info("[%d] Recording to file: %s", getpid(),file_name);
	g_object_set(filesink, "location", file_name, NULL);
	free(file_name);
	gst_bin_add_many(GST_BIN(pipeline), gst_object_ref(queue_record), gst_object_ref(encoder), gst_object_ref(muxer), gst_object_ref(filesink), NULL);
	gst_element_link_many(queue_record, encoder, muxer, filesink, NULL);
	gst_element_sync_state_with_parent(queue_record);
	gst_element_sync_state_with_parent(encoder);
	gst_element_sync_state_with_parent(muxer);
	gst_element_sync_state_with_parent(filesink);
	sinkpad = gst_element_get_static_pad(queue_record, "sink");
	gst_pad_link(teepad, sinkpad);
	gst_object_unref(sinkpad);
	recording = TRUE;
}

void sigintHandler(int sig) {
	log_info("[%d] Received: %d ", getpid(),sig);		
	if ( sig == 18 )
	{
		log_info("[%d] Received: SIGCONT (%d), start recording.", getpid(),sig);	
		if (!recording) {			
			startfile();
			signal(SIGCONT, sigintHandler);
		}
	}
	if ( sig == 10 ) 
	{
		log_info("[%d] Received: SIGUSR1 (%d), stop recording.", getpid(),sig);	
		if (recording) {
			stopfile();
			while ( unlinked == FALSE );
			unlinked=FALSE;
			signal(SIGSTOP, sigintHandler);
			
			 if ( recording == FALSE ) {
				char *targetfileanddirectory = (char *)malloc(sizeof(char) * 18); // TODO: Size
				sprintf(targetfileanddirectory,"%s/out.opus",targetdirectory);
				log_info("[%d] Moving to target directory: %s", getpid(),targetfileanddirectory);	
				rename("out.opus", targetfileanddirectory);				
			 }
			 
		}	
	}
}

int main(int argc, char *argv[])
{
	
	ini_t *config = ini_load(INI_FILE);
	ini_sget(config, "audiocomm", "targetdirectory", NULL, &targetdirectory);
	log_trace("[%d] audiocomm targetdirectory (%s) ",getpid(),targetdirectory);
	/* Create directories if they do not exist */
	struct stat st = {0};
	if (stat(targetdirectory, &st) == -1) {
		log_trace("[%d] Creating directory (%s) ",getpid(),targetdirectory);
		mkdir(targetdirectory, 0755);
	}



	signal(SIGCONT, sigintHandler); 
	signal(SIGUSR1, sigintHandler); 
	
	gst_init (&argc, &argv);
	pipeline = gst_pipeline_new(NULL);
	src = gst_element_factory_make("pulsesrc", NULL); 

	GstElement *capsfilter = gst_element_factory_make("capsfilter", NULL);
	GstCaps *caps = gst_caps_from_string ("audio/x-raw,channels=2"); // 1 44100
	g_object_set (capsfilter, "caps", caps, NULL);
	gst_caps_unref(caps);
	
	src_audioconvert = gst_element_factory_make ("audioconvert", NULL);
	src_resample  = gst_element_factory_make ("audioresample", NULL);
	tee = gst_element_factory_make("tee", NULL);
	audioconvert = gst_element_factory_make("audioconvert", NULL);	
	queue_fakesink = gst_element_factory_make("queue", "queue_fakesink");
	fakesink = gst_element_factory_make("fakesink", NULL);
	if (!pipeline || !src  || !tee ||  !audioconvert || !fakesink || !queue_fakesink || !capsfilter || !src_audioconvert || !src_resample) {
		log_error("[%d] Failed to create elements", getpid()); 
		return -1;
	}
	gst_bin_add_many(GST_BIN(pipeline), src,capsfilter,src_audioconvert,src_resample, tee, queue_fakesink, audioconvert, fakesink, NULL); // 
	if (!gst_element_link_many(src,capsfilter,src_audioconvert,src_resample, tee, NULL) || !gst_element_link_many(tee, queue_fakesink,audioconvert,fakesink, NULL) ) { // 
		log_error("[%d] Failed to link elements", getpid());
		return -2;
	}
	loop = g_main_loop_new(NULL, FALSE);
	bus = gst_pipeline_get_bus(GST_PIPELINE (pipeline));
	gst_bus_add_signal_watch(bus);
	g_signal_connect(G_OBJECT(bus), "message", G_CALLBACK(msghandle), NULL);
	gst_object_unref(GST_OBJECT(bus));
	gst_element_set_state(pipeline, GST_STATE_PAUSED); // GST_STATE_PAUSED GST_STATE_PLAYING
	log_info("[%d] audiocomm starting", getpid());
	log_info("[%d] start recording: 'kill -18 %d' or 'pkill -SIGCONT audiocomm' ", getpid(), getpid());
	log_info("[%d] stop  recording: 'kill -10 %d' or 'pkill -SIGUSR1 audiocomm' ", getpid(), getpid());
	g_main_loop_run(loop);
	return 0;
}
